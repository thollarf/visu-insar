# -*- coding: utf-8 -*-
"""
/***************************************************************************
 VisuInsar
                                 A QGIS plugin
 Visualization of Insar data
                              -------------------
        begin                : 2017-07-05
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Univ. Grenoble Alpes (Fr)
        email                : arthur.roussel@univ-grenobles-alpes.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

Plugin for visualisation of radar images.

At the moment, we can:

- load georeferenced radar images
- load radar image that gdal can read (and its geomap trans)
- visualize the image in a separated area
- select a subset of the data (in order to avoid outliers)
- visualize bands
- visualize the position of a point in the radar image in the
  images loaded in the "classic" qgis area.
"""

import os
import struct
import numpy as np
import copy

import osr
from osgeo import gdal

from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtGui, uic

from visu_insar_pointtool import VisuInsarPointTool
from visu_insar_dialog import VisuInsarDialog

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'visu_insar_dockwidget_base.ui'))

class VisuInsarDockWidget(QtGui.QDockWidget, FORM_CLASS):
    """
    Main code for the plugin visu insar. Creates a new drawing area in which we can
    see radar image.
    """
    closingPlugin = pyqtSignal()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(VisuInsarDockWidget, self).__init__(parent)
        self.iface = iface
        self.setupUi(self)
        self.canvasQGIS = self.iface.mapCanvas()

        # init attributes
        self.currentTool = "None" #ENUM {"cursor", "polygon", "..."} is a string telling which tool is currently in use
        self.tool = VisuInsarPointTool(self.canvas) #Custom tool used to get even from canvas
        self.trackToolLayer = None # Means that 'cursor tool' hasn't been called yet
        self.band1Shown=True
        self.band2Shown=True
        # contains a list of element, each of them being a list
        # [num_dataset, numband, [datasetvalues], qgist_raster_layer, gdal_dataset, [transform_matrix_X], [transform_matrix_Y], footprint]
        self.bandsAsArray=[]
        self.currentDataset=[]
        self.firstTimeDisplayBand=True
        self.activeLayer=None

        #Canvas events
        QObject.connect(self.tool, SIGNAL("moved"), self.toolMoved)
        QgsMapLayerRegistry.instance().layerWillBeRemoved[str].connect(self.removeLayerFromBandsArray)
        QObject.connect(self.canvasQGIS, SIGNAL("xyCoordinates(const QgsPoint &)"), self.trackQGIS)
        self.setDisableFunct(True)
        self.canvas.setMapTool(self.tool)

    def removeLayerFromBandsArray(self, text):
        layerNameRemoved = QgsMapLayerRegistry.instance().mapLayer(text).name()  # returns QgsMapLayer pointer
        index = 0
        while index < len(self.bandsAsArray):
            if self.bandsAsArray[index][3].name()==layerNameRemoved:
                del self.bandsAsArray[index]
                break
            index += 1
        self.updateComboBox()
        self.updateCurrentDataSet()
        self.canvas.setLayerSet([QgsMapCanvasLayer(self.currentDataset[0][3])])
        self.canvas.setExtent(self.currentDataset[0][3].extent())

    def updateCurrentDataSet(self):
        """ updates currentDataset with the bands of the
            last dataset in the bandsAsArray """
        self.currentDataset = [x for x in self.bandsAsArray if (x[0])==(self.bandsAsArray[-1][0])]

    def displayBand(self, boolBand1, boolBand2, threshold):
        """
           Callback link to the buttons band1, band2, spinbox .

           :param boolBandx: (booleand) True if bandx is activated
           :param threshold: (float) % of data to keep while computing color map.
        """
        self.updateCurrentDataSet()
        self.arrayLastBand1 = self.currentDataset[0][2]
        if len(self.currentDataset) == 2:
            self.arrayLastBand2 = self.currentDataset[1][2]
        # if band1 : black and white colormap else colormap
        if boolBand1 and not boolBand2:
            self.rendererBand1 = QgsSingleBandGrayRenderer(self.currentDataset[0][3].dataProvider(), 1)
            self.uses_band1 = self.rendererBand1.usesBands()
            # mimic the raster rendring properties dialog
            ce = QgsContrastEnhancement(self.currentDataset[0][3].dataProvider().dataType(0))
            ce.setContrastEnhancementAlgorithm(QgsContrastEnhancement.StretchToMinimumMaximum)
            stats = self.currentDataset[0][3].dataProvider().bandStatistics(self.uses_band1[0],QgsRasterBandStats.All,self.currentDataset[0][3].extent(),0)
            self.arrayLastBand1 = self.arrayLastBand1.ravel()
            self.arrayLastBand1.sort()
            unique_array = np.unique(self.arrayLastBand1) # Suppression des doublons & tri des valeurs
            if threshold>=99.9:
                nbElementSuppr = 1
            else:
                nbElementSuppr = (((100 - threshold) / 2) * len(unique_array)) / 100 # On mesure combien de données on va mettre de côté avec le seuil définit par l'utilisateur
                #nbElementSuppr = (stats.elementCount * ((100 - threshold)/2)) / 100
            ce.setMinimumValue(self.arrayLastBand1[nbElementSuppr])
            ce.setMaximumValue(self.arrayLastBand1[stats.elementCount-nbElementSuppr])
            self.rendererBand1.setContrastEnhancement(ce)
            self.currentDataset[0][3].setBlendMode(QPainter.CompositionMode_Multiply)
            self.currentDataset[0][3].setRenderer(self.rendererBand1)
            self.canvas.setLayerSet([QgsMapCanvasLayer(self.currentDataset[0][3])])
        elif not boolBand1 and boolBand2:
            try:
                #self.iface.legendInterface().setLayerVisible(self.currentDataset[0][3], False)
                #self.iface.legendInterface().setLayerVisible(self.currentDataset[1][3], False)
                self.uses_band2 = self.currentDataset[1][3].renderer().usesBands()
                stats = self.currentDataset[1][3].dataProvider().bandStatistics(self.uses_band2[0],QgsRasterBandStats.All,self.currentDataset[1][3].extent(),0)
                self.arrayLastBand2 = self.arrayLastBand2.ravel() # Mise à plat
                self.arrayLastBand2.sort()
                unique_array = np.unique(self.arrayLastBand2) # Suppression des doublons & tri des valeurs
                if threshold>=99.9:
                    nbEltSuppr = 1
                else:
                    nbEltSuppr = (((100 - threshold) / 2) * len(unique_array)) / 100 # On mesure combien de données on va mettre de côté avec le seuil définit par l'utilisateur

                max_arr = unique_array[len(unique_array)-nbEltSuppr]
                min_arr = unique_array[nbEltSuppr]

                s = QgsRasterShader()
                c = QgsColorRampShader()
                c.setColorRampType(QgsColorRampShader.INTERPOLATED)
                color_ramp = [QgsColorRampShader.ColorRampItem(min_arr, QtGui.QColor('#d7191c'), str(min_arr)),
                              QgsColorRampShader.ColorRampItem((max_arr-min_arr)*1/4+min_arr, QtGui.QColor('#fdae61'), str((max_arr-min_arr)*1/4+min_arr)),
                              QgsColorRampShader.ColorRampItem((max_arr-min_arr)*2/4+min_arr, QtGui.QColor('#ffffbf'), str((max_arr-min_arr)*2/4+min_arr)),
                              QgsColorRampShader.ColorRampItem((max_arr-min_arr)*3/4+min_arr, QtGui.QColor('#abdda4'), str((max_arr-min_arr)*3/4+min_arr)),
                              QgsColorRampShader.ColorRampItem(max_arr, QtGui.QColor('#2b83ba'), str(max_arr))
                             ]
                c.setColorRampItemList(color_ramp)
                s.setRasterShaderFunction(c)
                self.rendererBand2 = QgsSingleBandPseudoColorRenderer(self.currentDataset[1][3].dataProvider(), 2,  s)
                self.currentDataset[1][3].setRenderer(self.rendererBand2)
                self.canvas.setLayerSet([QgsMapCanvasLayer(self.currentDataset[1][3])])
            except IndexError:
                self.displayBand(True, False, 95)
        elif not boolBand1 and not boolBand2:
            self.iface.legendInterface().setLayerVisible(self.currentDataset[0][3], False)
            if len(self.currentDataset) == 2:
                self.iface.legendInterface().setLayerVisible(self.currentDataset[1][3], False)
                self.currentDataset[1][3].triggerRepaint()
            self.canvas.setLayerSet([])
        else:
            if len(self.currentDataset) == 1:
                self.canvas.setLayerSet([QgsMapCanvasLayer(self.currentDataset[0][3])])
            if len(self.currentDataset) == 2:
                self.canvas.setLayerSet([QgsMapCanvasLayer(self.currentDataset[0][3]),QgsMapCanvasLayer(self.currentDataset[1][3])])

            return
        for i in range(len(self.currentDataset)):
            self.currentDataset[i][3].triggerRepaint()
        self.canvas.refresh()

    # ================================= loading file tool ==========================================
    def loadTIF(self, file_name, is_geo_referenced):
        """
            Load the tif file from visu_insar_dialog.

            :param file_name: (str) the file name to open
            :param is_geo_referenced: (bool) true if file is georerenced, else false.
        """

        fileInfo = QFileInfo(file_name)
        baseName = fileInfo.baseName()

        dataSet = gdal.Open(file_name) # First open with gdal
        # if isGeoref -> don't need transmap
        if is_geo_referenced:

            # self.bandsAsArray contains informations about the datasets loaded.
            # For example, you will find informations about the first dataset loaded in : self.bandsAsArray[0]
            # self.bandsAsArray[0] returns [dataset number, band number, band as array, band as Raster, radarmatX as array, radarmatY as array, (shape)]


            # Disable prompt pop-up asking to set CRS
            # We backup settings in order to restore it after we loaded the layer
            oldValidation = QSettings().value("/Projections/defaultBehaviour")
            QSettings().setValue("/Projections/defaultBehaviour", "useGlobal")
            # TODO: refactor ?
            for band in range(dataSet.RasterCount): # For each band, read band as array and insert in list
                if len(self.bandsAsArray)==0:
                    raster = QgsRasterLayer(file_name, "0:"+str(band+1)+baseName)
                    # 4326 : code for WGS84
                    raster.setCrs(QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId))
                    struct_Dataset_Bandnumber_Band_Raster = [0,band+1,dataSet.GetRasterBand(band+1).ReadAsArray(), raster, dataSet, None, None]
                elif (band+1) == 1:
                    # first band -> increment dataset number
                    raster = QgsRasterLayer(file_name, str(self.bandsAsArray[-1][0]+1)+":"+str(band+1)+baseName)
                    raster.setCrs(QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId))
                    # Struct that save the number of the dataset, the number of the band and the band as an array
                    struct_Dataset_Bandnumber_Band_Raster = [self.bandsAsArray[-1][0]+1,band+1,dataSet.GetRasterBand(band+1).ReadAsArray(), raster, dataSet, None, None]
                else:
                    raster = QgsRasterLayer(file_name, str(self.bandsAsArray[-1][0])+":"+str(band+1)+baseName)
                    raster.setCrs(QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId))
                    struct_Dataset_Bandnumber_Band_Raster = [self.bandsAsArray[-1][0],band+1,dataSet.GetRasterBand(band+1).ReadAsArray(), raster, dataSet, None, None]
                self.bandsAsArray.append(struct_Dataset_Bandnumber_Band_Raster)

            self.iface.messageBar().clearWidgets() # to erase warnings : undefined CRS (?)


            #Restore olf config
            QSettings().setValue("/Projections/defaultBehaviour", oldValidation)

        else: # Load Tif and GeoMap Trans
            radarMatDataSet = copy.copy(self.currentRadarMat)
            for band in range(dataSet.RasterCount): # For each band, read band as array and insert in list
                if len(self.bandsAsArray) == 0:
                    raster = QgsRasterLayer(file_name, "0:"+str(band+1)+baseName)
                    struct_Dataset_Bandnumber_Band_Raster = [0,band+1,dataSet.GetRasterBand(band+1).ReadAsArray(), raster,
                                                             dataSet, radarMatDataSet.GetRasterBand(1).ReadAsArray(),
                                                             radarMatDataSet.GetRasterBand(2).ReadAsArray()]
                elif (band+1) == 1:
                    raster = QgsRasterLayer(file_name, str(self.bandsAsArray[-1][0]+1)+":"+str(band+1)+baseName)
                    # Struct that save the number of the dataset, the number of the band and the band as an array
                    struct_Dataset_Bandnumber_Band_Raster = [self.bandsAsArray[-1][0]+1,band+1,dataSet.GetRasterBand(band+1).ReadAsArray(),
                                                             raster, dataSet, radarMatDataSet.GetRasterBand(1).ReadAsArray(),
                                                             radarMatDataSet.GetRasterBand(2).ReadAsArray()]
                else:
                    raster = QgsRasterLayer(file_name, str(self.bandsAsArray[-1][0])+":"+str(band+1)+baseName)
                    struct_Dataset_Bandnumber_Band_Raster = [self.bandsAsArray[-1][0],band+1, dataSet.GetRasterBand(band+1).ReadAsArray(),
                                                             raster, dataSet, radarMatDataSet.GetRasterBand(1).ReadAsArray(),
                                                             radarMatDataSet.GetRasterBand(2).ReadAsArray()]
                self.bandsAsArray.insert(len(self.bandsAsArray),struct_Dataset_Bandnumber_Band_Raster)

            self.iface.messageBar().clearWidgets()

        # get qgis rasters and registry them, but do not them on the right hand side.
        liste = [x[3] for x in self.bandsAsArray]
        QgsMapLayerRegistry.instance().addMapLayers(liste)
        for layer in liste:
            self.iface.legendInterface().setLayerVisible(layer, False)

        self.canvas.setExtent(self.bandsAsArray[-1][3].extent())
        self.canvas.setLayerSet([QgsMapCanvasLayer(elt[3]) for elt in self.bandsAsArray])
        self.canvas.show()

        #Enable buttons after file is loaded
        self.setDisableFunct(False)
        self.updateComboBox()

    def loadRadarMat(self, file_name):
        " loads a radar mat using gdal."
        dataSet  =  gdal.Open(file_name)
        self.currentRadarMat = dataSet

    def test_event(self):
        layer = self.iface.activeLayer()
        print(layer.name())

    def loadFilesDialog(self):
        " Create new interface. User will choose which files to load from this interface"
        self.result = VisuInsarDialog(self)
        self.result.show()

    # ==============================================================================================

    def printValue(self, position):
        """
           Called when cursor is moved, update the coordinates label
         """
        positionLatLon = self.canvas.getCoordinateTransform().toMapCoordinates(position)
        self.updateCurrentDataSet()

        dataset = self.currentDataset
        info = []
        if self.currentDataset[0][5] is None:
            try:
                # Problemes : ne tient pas compte du nombre de bandes.
                # Va chercher les deux informations (bande 1 et bande 2) sur la premiere couche raster uniquement.
                if self.band1Shown and self.band2Shown:
                    ident1 = self.currentDataset[0][3].dataProvider().identify(self.tool.toMapCoordinates(self.canvas.mouseLastXY()), QgsRaster.IdentifyFormatValue)
                    ident2 = self.currentDataset[1][3].dataProvider().identify(self.tool.toMapCoordinates(self.canvas.mouseLastXY()), QgsRaster.IdentifyFormatValue)
                    res =  ident1.results().values()
                    info =  ident2.results().values()
                    info[0] = res[0]
                elif self.band1Shown and not self.band2Shown:
                    ident = self.currentDataset[0][3].dataProvider().identify(self.tool.toMapCoordinates(self.canvas.mouseLastXY()), QgsRaster.IdentifyFormatValue)
                    info =  ident.results().values()
                    info[1] = 0.0
                elif not self.band1Shown and self.band2Shown:
                    ident = self.currentDataset[1][3].dataProvider().identify(self.tool.toMapCoordinates(self.canvas.mouseLastXY()), QgsRaster.IdentifyFormatValue)
                    info =  ident.results().values()
                    info[0] = 0.0
                else:
                    ident = self.currentDataset[1][3].dataProvider().identify(self.tool.toMapCoordinates(self.canvas.mouseLastXY()), QgsRaster.IdentifyFormatValue)
                    info =  ident.results().values()
                    info[0] = 0.0
                    info[1] = 0.0

                self.labelCoord.setText('[%.3f ; %.3f] b1=%.5f b2=%.5f' % (positionLatLon.x(), positionLatLon.y(), info[0], info[1]))

            except TypeError:
                self.labelCoord.setText('[%.3f ; %.3f]' % (positionLatLon.x(), positionLatLon.y()) + " b1=null b2=null")
        else:
            # radar print value
            pass
    # ==============================================================================================


    # ================================= tracking cursor tool ==========================================
    # get coordinate from canvas
    def toolMoved(self, position):
        """ callback to signal moved (called when mouse moved) """
        if self.labelCoord.isEnabled():
            self.printValue(position)
        if self.currentTool == "cursor":
            self.track_tool(position)

    def track_tool(self, position): # TODO : adapter avec le geomaptrans
        """
            print cursor  in qgis layer from position in plugin layer.
            :param position: (tuple) position in plugin layer.
        """
        # get the layer at end of self.BandAsArray
        x = None
        y = None
        self.updateCurrentDataSet()
        self.activeLayer = self.currentDataset[0][3]
        position = self.canvas.getCoordinateTransform().toMapCoordinates(position)

        if self.trackToolLayer is None: # First time trackTool is called - Initialization
            self.trackToolLayer = QgsVectorLayer("Point?crs=epsg:4326", "Tracking Tool Layer", "memory")
            self.dataProCursor = self.trackToolLayer.dataProvider()
            self.dataProCursor.addAttributes([QgsField("id", QVariant.String), QgsField("lat", QVariant.Double), QgsField("long", QVariant.Double)])
            self.point = QgsPoint(0.0, 0.0)
            self.featureCursor = QgsFeature()

        if self.currentDataset[0][5] is None: # If is already georef
            x = position.x()
            y = position.y()
        else: # If is radar
            radarArrayX = self.currentDataset[0][6]
            radarArrayY = self.currentDataset[0][5]
            dataset = self.currentDataset[0][4]
            geotransform = dataset.GetGeoTransform()

            try:
                x= radarArrayX[abs(int(position[1]))][abs(int(position[0]))]
                y= radarArrayY[abs(int(position[1]))][abs(int(position[0]))]
            except IndexError:
                x= 0
                y= 0

        self.point.setX(x)
        self.point.setY(y)

        self.featureCursor.setGeometry(QgsGeometry.fromPoint(self.point))
        self.featureCursor.setAttributes(["id", x, y])

        listOfIds = [feat.id() for feat in self.dataProCursor.getFeatures()] #TODO Cleaner ....
        self.dataProCursor.deleteFeatures( listOfIds )

        self.dataProCursor.addFeatures([self.featureCursor])
        self.featureCursor.setAttributes(["id", x, y])
        QgsMapLayerRegistry.instance().addMapLayer(self.trackToolLayer)
        self.trackToolLayer.dataProvider().forceReload()
        self.trackToolLayer.triggerRepaint()
        self.iface.setActiveLayer(self.activeLayer)
    # =================================================================================================


    # =================================================================================================
    # Is a 'slot' linked to a radio button in QT designer
    def cursorReceived(self):
        self.setTool("cursor")


    # Change the tool currently in use. Only one tool can be used at the same time
    def setTool(self, string):
        self.currentTool=string
    # =================================================================================================


    # =================================================================================================
    def getShapeFromRaster(self, elementSelected):
        """
            returns the footprint of elementSelected as a list of 4 corners (in lat/lon coordinates).
        :param elementSelected: (list) a element of self.bandsAsArray.
        :return: the updated elementSelected
        """
        try: # if shape was already created
            shape = elementSelected[7]
            return elementSelected

        except IndexError: # else create shape and return
            shape=[]
            if elementSelected[5] is None:
                rasterArray = elementSelected[4].GetRasterBand(elementSelected[1]).ReadAsArray()
                shape.append(self.getMinX_MinY(rasterArray))
                shape.append(self.getMinX_MaxY(rasterArray))
                shape.append(self.getMaxX_MaxY(rasterArray))
                shape.append(self.getMaxX_MinY(rasterArray))
                elementSelected.append(shape)
                return elementSelected
            else:
                shape.append(self.getMinX_MinY_Radar(elementSelected))
                shape.append(self.getMinX_MaxY_Radar(elementSelected))
                shape.append(self.getMaxX_MaxY_Radar(elementSelected))
                shape.append(self.getMaxX_MinY_Radar(elementSelected))
                elementSelected.append(shape)
                return elementSelected

    def getMinX_MinY_Radar(self, elementSelected):
        """ find the first non null upper left value"""
        for x in xrange(elementSelected[5].shape[0]-1, 0, -5):
            for y in xrange(0, elementSelected[5].shape[1]-1, +10): # Large
                if elementSelected[5][x][y] != 0.0:
                    for sharpY in xrange(y-9,y,+1):                 # Sharp
                        if elementSelected[5][x][y] != 0.0:
                            return (x,y)

    def getMinX_MaxY_Radar(self, elementSelected):
        for y in xrange(elementSelected[5].shape[1]-1, 0, -5):
            for x in xrange(elementSelected[5].shape[0]-1, 0, -10):
                if elementSelected[5][x][y] != 0.0:
                    for sharpX in xrange(x+9, x, -1):
                        if elementSelected[5][x][y] != 0.0:
                            return (x,y)

    def getMaxX_MaxY_Radar(self, elementSelected):
        for x in xrange(0, elementSelected[5].shape[0]-1, +5):
            for y in xrange(elementSelected[5].shape[1]-1, 0, -10):
                if elementSelected[5][x][y] != 0.0:
                    for sharpY in xrange(y+9, y, -1):
                        if elementSelected[5][x][y] != 0.0:
                            return (x,y)

    def getMaxX_MinY_Radar(self, elementSelected):
        for y in xrange(0, elementSelected[5].shape[1]-1, +5):
            for x in xrange(0, elementSelected[5].shape[0]-1, +10):
                if elementSelected[5][x][y] != 0.0:
                    for sharpX in xrange(x-9, x, +1):
                        if elementSelected[5][x][y] != 0.0:
                            return (x,y)

    def getMinX_MinY(self, array):
        for x in xrange(0, array.shape[0]-1, +5):
                for y in xrange(0, array.shape[1]-1, +10): # Large
                    if array[x][y] != 0.0:
                        for sharpY in xrange(y-9,y,+1):                 # Sharp
                            if array[x][y] != 0.0:
                                return (x,y)
    def getMinX_MaxY(self,array):
        for y in xrange(array.shape[1]-1, 0, -5):
                for x in xrange(array.shape[0]-1, 0, -10):
                    if array[x][y] != 0.0:
                        for sharpX in xrange(x+9, x, -1):
                            if array[x][y] != 0.0:
                                return (x,y)
    def getMaxX_MinY(self,array):
        for y in xrange(0, array.shape[1]-1, +5):
                for x in xrange(0, array.shape[0]-1, +10):
                    if array[x][y] != 0.0:
                        for sharpX in xrange(x-9, x, +1):
                            if array[x][y] != 0.0:
                                return (x,y)
    def getMaxX_MaxY(self,array):
        for x in xrange(array.shape[0]-1, 0, -5):
                for y in xrange(array.shape[1]-1, 0, -10):
                    if array[x][y] != 0.0:
                        for sharpY in xrange(y+9, y, -1):
                            if array[x][y] != 0.0:
                                return (x,y)

    def updateComboBox(self):
        self.comboShape.clear()
        listOfRasterName=[x[3].name() for x in self.bandsAsArray]
        self.comboShape.addItems(listOfRasterName)

    def drawShape(self):
        """ get raster selected by user """
        rasterString = self.comboShape.currentText()
        mapLayers = [x.name() for x in self.iface.legendInterface().layers() if type(x) == QgsVectorLayer]
        # shape already defined, return
        if ("Shape"+rasterString) in mapLayers:
            return

        # looking for bandArray with name rasterString. Always successfull
        elementSelected = next(x for x in self.bandsAsArray if x[3].name() == rasterString)

        # == get shape from raster: get updated element with shape coordinates.
        result = self.getShapeFromRaster(elementSelected)

        # == create shapefile as QgsVectorLayer in QgsMapLayerRegistry
        shape = result[7]
        dataset = result[4]

        pointFeature = []
        listPoint = []

        # geotiff case: no coordinate transform
        if elementSelected[5] is None:
            geotransform = dataset.GetGeoTransform()
            for point in shape:
                x = geotransform[0] - point[1] * geotransform[5]
                y = geotransform[3] - point[0] * geotransform[1]
                listPoint.append(QgsPoint(x,y))
        else:
            for point in shape:
                x = elementSelected[6][point[0]][point[1]]
                y = elementSelected[5][point[0]][point[1]]
                listPoint.append(QgsPoint(x,y))

        # building shape: create lines out of the points.
        for index in range(len(listPoint)):
            feature = QgsFeature()
            if index == len(listPoint)-1:
                feature.setGeometry(QgsGeometry.fromPolyline([listPoint[index], listPoint[0]]))
                feature.setAttributes(["id", listPoint[index].x(), listPoint[index].y()])
                pointFeature.append(feature)
            else:
                feature.setGeometry(QgsGeometry.fromPolyline([listPoint[index], listPoint[index+1]]))
                feature.setAttributes(["id", listPoint[index].x(), listPoint[index].y()])
                pointFeature.append(feature)

        shapeLayer = QgsVectorLayer("LineString?crs=epsg:4326", "Shape"+rasterString, "memory")
        dataprovider=shapeLayer.dataProvider()
        dataprovider.addAttributes([QgsField("id", QVariant.String), QgsField("lat", QVariant.Double), QgsField("long", QVariant.Double)])

        dataprovider.addFeatures(pointFeature)
        QgsMapLayerRegistry.instance().addMapLayer(shapeLayer)
        shapeLayer.dataProvider().forceReload()
        shapeLayer.triggerRepaint()

    # =================================================================================================


    def band1Canvas(self):
        """events caught when user wants to display bands """
        if self.band1Shown:
            self.band1Shown=False
        else:
            self.band1Shown=True
        self.displayBand(self.band1Shown, self.band2Shown, self.thresholdBox.value())

    def band2Canvas(self):
        if self.band2Shown:
            self.band2Shown=False
        else:
            self.band2Shown=True
        self.displayBand(self.band1Shown, self.band2Shown, self.thresholdBox.value())

    def sendSignalToDisplayBand(self):
        self.displayBand(self.band1Shown, self.band2Shown, self.thresholdBox.value())

    # =================================================================================================


    def trackQGIS(self, position):
        #print(str(position))
        pass

    def setDisableFunct(self, boolean):
        """ activate of de-activate ui elements
        :param boolean:  if False deactivate elements, else activate
        """
        self.labelCoord.setDisabled(boolean)
        self.band1.setDisabled(boolean)
        self.band2.setDisabled(boolean)
        self.radioCursor.setDisabled(boolean)
        self.thresholdBox.setDisabled(boolean)
        self.comboShape.setDisabled(boolean)
        self.shapeButton.setDisabled(boolean)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

