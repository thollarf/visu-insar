# -*- coding: utf-8 -*-
"""
/***************************************************************************
 VisuInsar
                                 A QGIS plugin
 Visualization of Insar data
                              -------------------
        begin                : 2017-07-05
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Arthur
        email                : arthur.roussel@univ-grenobles-alpes.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import * 
from qgis.gui import *

from PyQt4 import QtGui, uic

class VisuInsarPointTool(QgsMapTool):   
    def __init__(self, canvas):
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas

    def canvasPressEvent(self, event):
        x = event.pos().x()
        y = event.pos().y()
        self.emit( SIGNAL("pressed"), QPoint(x, y) )

    def canvasMoveEvent(self, event):  
        x = event.pos().x()
        y = event.pos().y()

        #point = self.canvas.getCoordinateTransform().toMapCoordinates(x, y)
        #print("point :"+str(point)+" (x="+str(point.x())+";y="+str(point.y())+")")
        
        self.emit( SIGNAL("moved"), QPoint(x, y) )


    def canvasReleaseEvent(self, event):
        #Get the click
        x = event.pos().x()
        y = event.pos().y()

        point = self.canvas.getCoordinateTransform().toMapCoordinates(x, y)
        print(self.canvas.getCoordinateTransform())

    def activate(self):
        pass

    def deactivate(self):
        pass

    def isZoomTool(self):
        return False

    def isTransient(self):
        return False

    def isEditTool(self):
        return True
