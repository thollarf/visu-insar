# -*- coding: utf-8 -*-
"""
/***************************************************************************
 VisuInsar
                                 A QGIS plugin
 Visualization of Insar data
                             -------------------
        begin                : 2017-07-05
        copyright            : (C) 2017 by Arthur
        email                : arthur.roussel@univ-grenobles-alpes.fr
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load VisuInsar class from file VisuInsar.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .visu_insar import VisuInsar
    return VisuInsar(iface)
