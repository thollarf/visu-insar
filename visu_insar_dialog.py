# -*- coding: utf-8 -*-
"""
/***************************************************************************
 VisuInsarDialog
                                 A QGIS plugin
 Visualization of Insar data
                              -------------------
        begin                : 2017-07-05
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Arthur
        email                : arthur.roussel@univ-grenobles-alpes.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *

from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'visu_insar_dialog.ui'))

class ErrorRadarMat(Exception):
    def __init__(self, message):
        super(ErrorRadarMat, self).__init__(message)

class ErrorFileNameTIF(Exception):
    def __init__(self, message):
        super(ErrorFileNameTIF, self).__init__(message)


class VisuInsarDialog(QtGui.QDialog, FORM_CLASS):


    def openTIF(self):
        self.label.setText("")
        self.fileNameTIF=QFileDialog.getOpenFileName(self, "Select a TIF file", "/home/roussela/Documents/donnees", '*.tif *.tiff')
        #self.fileNameTIF="/home/roussela/Documents/donnees/16rlks/geomap_16rlks.tif"
        fileInfo = QFileInfo(self.fileNameTIF)
        filename=fileInfo.fileName()
        self.lineEdit.setText(filename)
        if filename[:3]=="geo":
            self.isGeo.setCheckState(2)

    def openRadarMat(self):
        self.label.setText("")
        self.fileNameRadarMat=QFileDialog.getOpenFileName(self, "Select a TIF file", "/home/roussela/Documents/donnees", '*.tif *.tiff')
        #self.fileNameTransMat="/home/roussela/Documents/donnees/16rlks/geomap_16rlks.tif"

    def accept(self):
        try:
            # Envoi de geotrans
            if not self.isGeo.isChecked():
                if self.fileNameRadarMat=="": # Si l'utilisateur clique sur cancel
                    raise ErrorRadarMat("")
                else:
                    self.iface.loadRadarMat(self.fileNameRadarMat)
            # Envoi de .tif
            if self.fileNameTIF=="": # Si l'utilisateur clique sur cancel
                raise ErrorFileNameTIF("")
            else: #Si le nom n'est pas vide
                self.iface.loadTIF(self.fileNameTIF, self.isGeo.isChecked())

        except(ErrorFileNameTIF):
            self.label.setText("Please select a valid .tif file")
        except(ErrorRadarMat):
            self.label.setText("Please select a valid radar mat file")
        except(AttributeError):
            self.label.setText("Error :: can't submit .tif or geotrans")
        else:
            self.close()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(VisuInsarDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.iface=iface

