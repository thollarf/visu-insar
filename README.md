Visualization of InSar data
===========================

Author(s)
--------
Arthur Roussel and al. 

Features:
=========
Visu-insar is a plugin for visualizing insar data. 
It loads image in a separate area and allow to visualise the position
in parallel in the other image. 

Bands can be visualized separately, and outliers can be eliminated in order to have a more
relevant color scale. 

Supported image formats:
----------------------

Images that are nativelly supported by gdal can be loaded by the plugin. 
Georeferenced images are positionned in the space. 

Images that are not georeferenced can be loaded provided the user can provide a coordinate
translation between pixel in the image and latlong coordinate. Such a translation map is called
geomaptrans. 

Supported formats are:

   - '.unw','Unwrapped files (*.unw)','BIL','2','real','float32','SAR amplitude image and unwrapped phase' };
   - '.dem','DEMs (*.dem)','1Band','1','real','int16','digital elevation model in lat-long coordinates'; ...
   - '.slc','Single Look Complex (*.slc)','BIP','2','cpx_real_img','float32','real and imaginary parts of single-look complex image (and its multilook versions)'; ...

Future handled formats:
-----------------------

   - '.int','Interferograms (*.int)','BIP','2','cpx_real_img','float32','real and imaginary parts of interferogram'; ...
   - '.hgt','Height files (*.hgt)','BIL','2','real','float32','simulated SAR amplitude and elevation in radar coordinates'; ....
   - '.byt','Byte files (*.byt)','1Band','1','real','int8','1-byte signed integer'; ...
   - '.ci2','complex image (*.ci2)','BIP','2','cpx_real_img','int16','real and imaginary parts of SLC (Diapason format)'; ...
   - '.cor','Correlation files (*.cor)','BIL','2','real','float32','average amplitude of SAR images used to form an interferogram and correlation measure of coherence'; ...
   - '.flg','Flag files (*.flg)','1Band','1','real','int8','flags used and resulting from unwrapping with roipac unwrapper, 1 byte values with flags set in bits'; ...
   - '.msk','Mask files (*.msk)','BIL','2','real','float32','SAR amplitude image and coherence with zeros in masked out areas'; ...
   - '.off','Offset files (*.off)','TEXT','8','real','float32','Ascii text of offsets measured between two images'; ...
   - '.r4', 'float32 files (*.r4)','1Band','1','real','float32','Binary file, real 4-byte values (1 band)' ;
   - '.raw','raw files (*.raw)','BIP','2','cpx_real_img','int8','I/Q values for each echo sample of not focused radar image';...
   - '.rmg','rmg files (*.r4)','BIL','2','real','float32','Binary file, 2 bands, real 4-byte values band interleaved by line (BIL)' ;
   - '.slp','Slope files (*.slp)','BIL','2','real','float32','slope and azimuth of the slope corresponding to DEM file'; ...
   - '.trans','geomap_trans file (*.trans)','BIL','2','real','float32','inverse mapping transformation from SAR to DEM coordinates, two bands are range and azimuth pixel locations of
SAR for each DEM pixel' +; ...

Installation:
============

Installation is done through the cloning of the plugin in the directory 
.qgis2/python/plugins 

Usage:
======

Once qgis is loaded the plugin appears in the plugins icons as a circular fringe. 
When clicking on the plugin button, a dock shows up where images can be loaded. 

By clicking on the "load" button on the plugin dock we are firing a dialog that allow 
to search for an image to visualize. If the image is not georererenced, a geomaptrans 
file must be given. 



